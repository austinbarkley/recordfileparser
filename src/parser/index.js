import ParserReturn from "./models/ParserReturn";

export default (lineString, numOfFields, stringDeliminator) => {
  const wordsArray = lineString.split(stringDeliminator);

  const isValid = wordsArray.length === numOfFields;

  return new ParserReturn(isValid, lineString + "\n");
};
