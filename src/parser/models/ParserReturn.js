export default class ParserReturn {
  constructor(isValid, data) {
    this.isValid = isValid;
    this.data = data;
  }
}
