import chalk from "chalk";

/**
 * error - Console log error messages in red text
 * @param {string} str - String to log
 */
export const error = str => {
  console.error(chalk.red(str));
};

/**
 * debug - Console log debug messages in yellow text
 * @param {string} str - String to log
 */
export const debug = str => {
  console.debug(chalk.yellow(str));
};

/**
 * info - Console log info messages in white text
 * @param {string} str - String to log
 */
export const info = str => {
  console.log(chalk.white(str));
};

/**
 * start - Console log starting application message
 * @param {string} str - String to log
 */
export const start = str => {
  console.log(chalk.green(str));
};
