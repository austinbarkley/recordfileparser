import fs from "fs";
import readlineSync from "readline-sync";
import path from "path";
import { error } from "../helpers/text/colors";

/**
 * Default exported function that runs all prompt steps and return the user
 * answer object.
 *
 * @returns {Object} userAnswers - Object containing the user's answers.
 * @returns {string} userAnswers.fileLocation - user's inputted file location.
 * @returns {string} userAnswers.FileType - user inputted file type.
 * @returns {Number} userAnswers.numFields - user inputted number of fields.
 */
export default () => {
  const fileLocation = getFileLocation();

  console.log(); // Empty line after text input
  const fileType = getFileType();

  console.log(); // Empty line after text input
  const numFields = getNumberOfFields();

  return {
    fileLocation,
    fileType,
    numFields
  };
};

/**
 * getFileLocation - User Prompt step to get the user's file location.
 * Will check if path exists to file.
 *
 * @returns {string} file location.
 */
const getFileLocation = () => {
  console.log("Where is the file location?");

  let fileLocation = "";
  const handleFileLocation = () => {
    readlineSync.promptLoop(answer => {
      if (fs.existsSync(answer)) {
        fileLocation = answer;
        return answer;
      }

      error("Please enter a valid file location:");
    });
  };

  handleFileLocation();
  return fileLocation;
};

/**
 * getFileType - User prompt step to get the user's file type.
 * Will only allow CSV or TSV.
 *
 * @returns {string} user choice of CSV or TSV.
 */
const getFileType = () => {
  console.log(
    "Is the file format CSV (comma-separated values) or TSV (tab-separated values)?"
  );

  let FileType = "";
  const handleGetFileType = () => {
    readlineSync.promptLoop(answer => {
      if (answer.toUpperCase() === "CSV" || answer.toUpperCase() === "TSV") {
        FileType = answer.toUpperCase();
        return answer.toUpperCase();
      }

      error("Please enter either CSV or TSV:");
    });
  };

  handleGetFileType();
  return FileType;
};

/**
 * getNumberOfFields - User Prompt step for number of fields in the file.
 * Will only allow a number input.
 *
 * @returns user input of number of fields a good row should have.
 */
const getNumberOfFields = () => {
  console.log("How many fields should each record contain?");

  let numFields = "";
  const handleGetNumFields = () => {
    readlineSync.promptLoop(answer => {
      if (!isNaN(answer)) {
        numFields = Number(answer);
        return answer;
      }

      error("Please enter a valid number:");
    });
  };

  handleGetNumFields();
  return numFields;
};
