import fs from "fs";

const goodFile = "goodParsedFile.txt";
const badFile = "badParsedFile.txt";

export default ({ isValid, data }) => {
  let fileName = "";
  isValid ? (fileName = goodFile) : (fileName = badFile);

  fs.appendFileSync(fileName, data);
};
