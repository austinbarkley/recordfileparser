#!/usr/bin/env node
import { error, info, start } from "./helpers/text/colors";
import path from "path"; // TODO: remove after debugging
import promptUser from "./prompt";
import Parser from "./parser";
import writer from "./writer";
import fs from "fs";

const main = () => {
  start("\n#### Starting RecordFileParser ####\n");

  const userAnswers = promptUser();

  const stringDelimiterResolver = () => {
    switch (userAnswers.fileType) {
      case "CSV":
        return ",";
      case "TSV":
        return "\t";
      default:
        throw new Error("Unknown File Type Chosen");
        break;
    }
  };

  const stringDelimiter = stringDelimiterResolver();

  // Work on data
  info("Parsing file... \n");
  try {
    const content = fs.readFileSync(
      path.join(process.cwd(), userAnswers.fileLocation),
      {
        encoding: "utf8"
      }
    );

    const linesArray = content.split(/\r?\n/).splice(1);
    linesArray.forEach(lineString => {
      const parserReturn = Parser(
        lineString,
        userAnswers.numFields,
        stringDelimiter
      );

      writer(parserReturn);
    });
  } catch (err) {
    error("An error occured while try to parse file", err);
  }

  process.exit(0);
};

main();
